package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/hackebrot/tweets/twitter"
)

func main() {
	track := flag.String("track", "#FOSS,golang blog", "a comma-separated list of phrases to track")
	flag.Parse()

	s, err := twitter.NewStream(os.Stdout, track)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	defer s.Close()

	// Wait for CTRL-C
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch
}
